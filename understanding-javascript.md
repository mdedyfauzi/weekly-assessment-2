# Task 1: Understanding Javascript

**The Questions**

1. What is the difference between **Javascript** and **HTML**?
2. What is the differenece between `var`, `let`, and `const`?
3. What **data types** in javascript do you know?
4. What do you know about `function`?
5. What do you know about **hoisting**?

**The Answers**

1. The difference is **Javascript** was a programming language which is widely used for web development. **HTML** is a Markup Language in which a format in writing where the text will be processed into display.
2. The difference is `var` declarations are globally scoped or function/locally scoped and for the `let`, `let` is block scoped where a chunk of code bounded by {}. A block lives in curly braces. Anything within curly braces is a block.The last one is `const`, `const` declarations are block scoped it means const can only be accessed within the block they were declared.
   const cannot be updated or re-declared.
3. **Data Types** in javascript:
   - Primitives Data Type :
     - Char
     - String
     - Text
     - Integer
     - Float
     - Double
     - Boolean
   - Higher Level Data Types :
     - Array
     - Object
4. A `function` is a block of code organized to perform an action. this code is made to be reusable.
5. **Hoisting** is JavaScript's default behavior of moving all declarations to the top of the current scope (to the top of the current script or the current function).

## References

- [Var, Let, and Const – What's the Difference?](https://www.freecodecamp.org/news/var-let-and-const-whats-the-difference/)
- [JavaScript Hoisting](https://www.w3schools.com/js/js_hoisting.asp)
