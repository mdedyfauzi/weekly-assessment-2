// write function here
function printNumberAscending(number) {
  for (var i = 0; i <= number; i++) {
    console.log(i);
  }
  return number;
}

// input test
const input1 = 5;
const input2 = 10;

printNumberAscending(input1); // output: 0 1 2 3 4 5
